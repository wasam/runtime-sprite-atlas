﻿/*
 * Open Sprite Atlas
 * a very minimalistic replacement for the SpriteAtlas class found in Unity 
 * (https://docs.unity3d.com/ScriptReference/U2D.SpriteAtlas.html)
 * 
 * 2020 by Samuel Walz <mail@samwalz.com>
 */

using System.Collections.Generic;
using UnityEngine;
using DaVikingCode.RectanglePacking;

namespace Wasam.RuntimeSpriteAtlas
{
    public class OpenSpriteAtlas
    {
        private const int Padding = 2;
        private const int MaxTextureSize = 2048;

        private readonly int _padding;
        private readonly int _maxTextureSize;
        private readonly TextureFormat _textureFormat;
        private readonly List<Texture2D> _atlasTextures = new List<Texture2D>();
        private readonly List<Sprite> _spriteList = new List<Sprite>();
        private readonly Dictionary<string, List<Sprite>> _spritesByName = new Dictionary<string, List<Sprite>>();

        public int SpriteCount => _spriteList.Count;

        public OpenSpriteAtlas(
            Sprite[] textures,
            int padding = Padding,
            int maxTextureSize = MaxTextureSize,
            TextureFormat textureFormat = TextureFormat.ARGB32)
        {
            _padding = padding;
            _maxTextureSize = maxTextureSize;
            _textureFormat = textureFormat;
            while (textures.Length > 0)
            {
                PackTextures(textures, out var atlasTexture, out var sprites, out textures);
                _atlasTextures.Add(atlasTexture);
                Add(sprites);
            }
        }
        
        
        /// <summary>
        ///
        /// </summary>
        /// <param name="textures"></param>
        /// <param name="padding"></param>
        /// <param name="maxTextureSize"></param>
        /// <param name="textureFormat">use same as format for textures</param>
        public OpenSpriteAtlas(
            Texture2D[] textures,
            int padding = Padding,
            int maxTextureSize = MaxTextureSize,
            TextureFormat textureFormat = TextureFormat.ARGB32)
        {
            _padding = padding;
            _maxTextureSize = maxTextureSize;
            _textureFormat = textureFormat;
            while (textures.Length > 0)
            {
                PackTextures(textures, out var atlasTexture, out var sprites, out textures);
                _atlasTextures.Add(atlasTexture);
                Add(sprites);
            }
        }
        
        private void PackTextures(
            IReadOnlyList<Sprite> textures,
            out Texture2D atlasTexture,
            out Sprite[] sprites,
            out Sprite[] leftovers)
        {
            // pack rectangles
            var rpack = new RectanglePacker(_maxTextureSize, _maxTextureSize);
            for (var i = 0; i < textures.Count; i++)
            {
                var tex = textures[i].texture;
                rpack.InsertRectangle(tex.width + _padding * 2, tex.height + _padding * 2, i);
            }
            rpack.PackRectangles();

            // prepare atlas texture
            atlasTexture = GetColoredTexture(rpack.PackedWidth, rpack.PackedHeight, Color.clear, _textureFormat);
            
            // sort textures
            var leftoverList = new List<Sprite>();
            var copyList = new List<CopyInstruction>();
            for (var i = 0; i < textures.Count; i++)
            {
                var sourceTexture = textures[i];
                var rectangle = rpack.GetRectangleByID(i);
                if (rectangle == null)
                {
                    // texture can not be packed this time -> leftover
                    leftoverList.Add(sourceTexture);
                }
                else
                {
                    copyList.Add(new CopyInstruction
                    {
                        Rect = new IntegerRectangle(
                            rectangle.x + _padding, 
                            rectangle.y + _padding,
                            sourceTexture.texture.width,
                            sourceTexture.texture.height
                            ),
                        Source = sourceTexture.texture,
                        Pivot = new Vector2(
                            sourceTexture.pivot.x / sourceTexture.texture.width,
                            sourceTexture.pivot.y / sourceTexture.texture.height
                            ),
                        Name = sourceTexture.name
                    });
                }
            }
            leftovers = leftoverList.ToArray();
            
            // copy textures into place & create sprites
            CopyTextures(copyList, atlasTexture, _padding > 0);
            
            // create sprites
            sprites = CreateSprites(copyList, atlasTexture);
        }
        
        private void PackTextures(
            IReadOnlyList<Texture2D> textures,
            out Texture2D atlasTexture,
            out Sprite[] sprites,
            out Texture2D[] leftovers)
        {
            // pack rectangles
            var rpack = new RectanglePacker(_maxTextureSize, _maxTextureSize);
            for (var i = 0; i < textures.Count; i++)
            {
                var tex = textures[i];
                rpack.InsertRectangle(tex.width + _padding * 2, tex.height + _padding * 2, i);
            }
            rpack.PackRectangles();

            // prepare atlas texture
            atlasTexture = GetColoredTexture(rpack.PackedWidth, rpack.PackedHeight, Color.clear, _textureFormat);
            
            // sort textures
            var leftoverList = new List<Texture2D>();
            var copyList = new List<CopyInstruction>();
            for (var i = 0; i < textures.Count; i++)
            {
                var sourceTexture = textures[i];
                var rectangle = rpack.GetRectangleByID(i);
                if (rectangle == null)
                {
                    // texture can not be packed this time -> leftover
                    leftoverList.Add(sourceTexture);
                }
                else
                {
                    copyList.Add(new CopyInstruction
                    {
                        Rect = new IntegerRectangle(
                            rectangle.x + _padding, 
                            rectangle.y + _padding,
                            sourceTexture.width,
                            sourceTexture.height
                            ),
                        Source = sourceTexture,
                        Pivot = Vector2.one * .5f,
                        Name = sourceTexture.name
                    });
                }
            }
            leftovers = leftoverList.ToArray();
            
            // copy textures into place & create sprites
            CopyTextures(copyList, atlasTexture, _padding > 0);
            
            // create sprites
            sprites = CreateSprites(copyList, atlasTexture);
        }

        private static Sprite[] CreateSprites(List<CopyInstruction> copyInstructions, Texture2D atlasTexture)
        {
            var sprites = new Sprite[copyInstructions.Count];
            for (var i = 0; i < copyInstructions.Count; i++)
            {
                sprites[i] = CreateSprite(copyInstructions[i], atlasTexture);
            }
            return sprites;
        }
        private static Sprite CreateSprite(CopyInstruction instruction, Texture2D atlasTexture)
        {
            var sprite = Sprite.Create(
                atlasTexture, 
                instruction.Rect.Rect, 
                instruction.Pivot,
                1, 
                0,
                SpriteMeshType.FullRect);
            sprite.name = instruction.Name;
            return sprite;
        }
        private static void CopyTextures(List<CopyInstruction> instructions, Texture2D atlasTexture, bool border)
        {
            if (Application.platform == RuntimePlatform.WebGLPlayer &&
                !Application.isEditor)
            {
                CopyTexturesViaPixels(instructions, atlasTexture, border);
            }
            else
            {
                CopyTexturesViaGraphics(instructions, atlasTexture, border);
            }
        }
        private static void CopyTexturesViaPixels(List<CopyInstruction> instructions, Texture2D atlasTexture, bool border)
        {
            var atlasPixels = atlasTexture.GetPixels32();
            foreach (var instruction in instructions)
            {
                var sourceTexture = instruction.Source;
                var rectangle = instruction.Rect;
                
                CopyTexture(sourceTexture, atlasPixels, atlasTexture.width, rectangle.x, rectangle.y);
                if (border)
                {
                    var srcWidth = sourceTexture.width;
                    var srcHeight = sourceTexture.height;
                    
                    // add on top
                    CopyTexture(
                        sourceTexture, 0, srcHeight - 1, srcWidth, 1,
                        atlasPixels, atlasTexture.width, rectangle.x, rectangle.y + srcHeight
                    );
                    // add on bottom
                    CopyTexture(
                        sourceTexture, 0, 0, srcWidth, 1,
                        atlasPixels, atlasTexture.width, rectangle.x, rectangle.y - 1
                    );
                    // add left
                    CopyTexture(
                        sourceTexture, 0, 0, 1, srcHeight,
                        atlasPixels, atlasTexture.width, rectangle.x - 1, rectangle.y
                    );  
                    // add right
                    CopyTexture(
                        sourceTexture, srcWidth - 1, 0, 1, srcHeight,
                        atlasPixels, atlasTexture.width, rectangle.x + srcWidth, rectangle.y
                    );
                }
            }
            atlasTexture.SetPixels32(atlasPixels);
            atlasTexture.Apply();
        }
        private static void CopyTexturesViaGraphics(List<CopyInstruction> instructions, Texture2D atlasTexture, bool border)
        {
            foreach (var instruction in instructions)
            {
                var sourceTexture = instruction.Source;
                var srcWidth = sourceTexture.width;
                var srcHeight = sourceTexture.height;
                var rectangle = instruction.Rect;
                
                // check texture format
                if (sourceTexture.format != atlasTexture.format)
                {
                    var tmpTex = new Texture2D(srcWidth, srcHeight, atlasTexture.format, false);
                    var pixels = sourceTexture.GetPixels32();
                    tmpTex.SetPixels32(pixels);
                    tmpTex.Apply();
                    sourceTexture = tmpTex;
                }
                Graphics.CopyTexture(
                    sourceTexture, 0, 0, 0, 0, srcWidth, srcHeight,
                    atlasTexture, 0, 0, rectangle.x, rectangle.y
                );
                if (border)
                {
                    // add on top
                    Graphics.CopyTexture(
                        sourceTexture, 0, 0, 0, srcHeight - 1, srcWidth, 1,
                        atlasTexture, 0, 0, rectangle.x, rectangle.y + srcHeight
                    );
                    // add on bottom
                    Graphics.CopyTexture(
                        sourceTexture, 0, 0, 0, 0, srcWidth, 1,
                        atlasTexture, 0, 0, rectangle.x, rectangle.y - 1
                    );
                    // add left
                    Graphics.CopyTexture(
                        sourceTexture, 0, 0, 0, 0, 1, srcHeight,
                        atlasTexture, 0, 0, rectangle.x - 1, rectangle.y
                    );  
                    // add right
                    Graphics.CopyTexture(
                        sourceTexture, 0, 0, srcWidth - 1, 0, 1, srcHeight,
                        atlasTexture, 0, 0, rectangle.x + srcWidth, rectangle.y
                    );
                }
            }
        }
        private static void CopyTexture(Texture2D src, Color32[] dstPixels, int dstWidth, int dstX, int dstY)
        {
            var srcWidth = src.width;
            var srcHeight = src.height;
            CopyTexture(src, 0, 0, srcWidth, srcHeight, dstPixels, dstWidth, dstX, dstY);
        }
        private static void CopyTexture(Texture2D src, int srcX, int srcY, int srcWidth, int srcHeight, Color32[] dstPixels, int dstWidth, int dstX, int dstY)
        {
            var srcPixels = src.GetPixels32();
            // copy all pixels into target
            for (var x = 0; x < srcWidth; x++)
            {
                for (var y = 0; y < srcHeight; y++)
                {
                    var srcIndex = srcX + x + srcWidth * (srcY + y);
                    var dstIndex = dstX + x + dstWidth * (dstY + y);
                    dstPixels[dstIndex] = srcPixels[srcIndex];
                }
            }
        }

        private void Add(Sprite[] sprites)
        {
            for (var i = 0; i < sprites.Length; i++)
            {
                Add(sprites[i]);
            }
        }
        private void Add(Sprite sprite)
        {
            _spriteList.Add(sprite);
            var spriteName = sprite.name;
            if (!_spritesByName.ContainsKey(spriteName))
            {
                _spritesByName.Add(spriteName, new List<Sprite>());
            }
            _spritesByName[spriteName].Add(sprite);
        }
        public Sprite GetSprite(string name)
        {
            return _spritesByName.TryGetValue(name, out var sprite) ? Clone(sprite[0]) : null;
        }
        public void GetSprites(Sprite[] sprites)
        {
            if (_spriteList.Count != sprites.Length)
            {
                throw new System.Exception("array has wrong size");
            }
            for (var i = 0; i < sprites.Length; i++)
            {
                var sprite = _spriteList[i];
                sprites[i] = Clone(sprite);
            }
        }
        private static Sprite Clone(Sprite sprite)
        {
            var spriteClone = Sprite.Create(
                sprite.texture, 
                sprite.rect, 
                new Vector2(sprite.pivot.x / sprite.rect.width, sprite.pivot.y / sprite.rect.height), 
                sprite.pixelsPerUnit
                );
            spriteClone.name = sprite.name;
            return spriteClone;
        }
        private static Texture2D GetColoredTexture(int width, int height, Color32 color, TextureFormat textureFormat)
        {
            var result = new Texture2D(width, height, textureFormat, false)
            {
                //filterMode = FilterMode.Point,
                name = color.ToString()
            };
            var pixels = result.GetPixels32();
            for (var i = 0; i < pixels.Length; i++)
            {
                pixels[i] = color;
            }
            result.SetPixels32(pixels);
            result.Apply();
            return result;
        }
        
        private struct CopyInstruction
        {
            public IntegerRectangle Rect;
            public Texture2D Source;
            public Vector2 Pivot;
            public string Name;
        }
    }
}

