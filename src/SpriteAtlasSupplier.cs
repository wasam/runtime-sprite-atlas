﻿/*
 * Sprite Atlas Supplier
 * creates OpenSpriteAtlas from external image files
 * 
 * - searches directories at supplied path recursively for image files
 * 
 * 2020 by Samuel Walz <mail@samwalz.com>
 */ 
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace Wasam.RuntimeSpriteAtlas
{
    public class SpriteAtlasSupplier : MonoBehaviour
    {
        public delegate void SpriteAtlasReadyHandler(OpenSpriteAtlas spriteAtlas);

        public int maxTextureSize = 4096;
        public string[] preloadAtlases;

        private bool _initialised;

        private static SpriteAtlasSupplier _instance;

        private readonly Dictionary<string, OpenSpriteAtlas> _spriteAtlasesByPath = new Dictionary<string, OpenSpriteAtlas>();
        private readonly Dictionary<string, List<SpriteAtlasReadyHandler>> _requestsByPath = new Dictionary<string, List<SpriteAtlasReadyHandler>>();

        public static SpriteAtlasSupplier Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<SpriteAtlasSupplier>();
                    if (_instance == null)
                    {
                        var go = new GameObject("sprite_atlas_supplier");
                        _instance = go.AddComponent<SpriteAtlasSupplier>();
                    }
                    _instance.Init();
                }
                return _instance;
            }
        }

        public void RequestSpriteAtlas(string path, SpriteAtlasReadyHandler onSpriteAtlasReady)
        {
            if (_spriteAtlasesByPath.TryGetValue(path, out var spriteAtlas))
            {
                onSpriteAtlasReady.Invoke(spriteAtlas);
            }
            else
            {
                if (!_requestsByPath.ContainsKey(path))
                {
                    _requestsByPath.Add(path, new List<SpriteAtlasReadyHandler>());
                    StartCoroutine(SpriteAcquisitionRoutine(path));
                }
                _requestsByPath[path].Add(onSpriteAtlasReady);
            }
        }

        private IEnumerator SpriteAcquisitionRoutine(string requestedPath)
        {
            var textures = new List<Texture2D>();

            // gather file paths
            var path = FullPath(requestedPath, true);
            var filePaths = FindFiles(path, new string[] { "*.png", "*.jpg", "*.jpeg" }, true);
            
            // load all textures
            for (var i = 0; i < filePaths.Length; i++)
            {
                var filePath = "file://" + filePaths[i];
                using var wwwLoadImage = UnityWebRequestTexture.GetTexture(filePath);
                yield return wwwLoadImage.SendWebRequest();
                if (!string.IsNullOrEmpty(wwwLoadImage.error)) continue;
                
                var image = DownloadHandlerTexture.GetContent(wwwLoadImage);
                image.name = Path.GetFileNameWithoutExtension(filePath);
                textures.Add(image);
            }

            // pack
            var spriteAtlas = new OpenSpriteAtlas(textures.ToArray(), 2, maxTextureSize);
            _spriteAtlasesByPath.Add(requestedPath, spriteAtlas);

            // distribute
            if (_requestsByPath.TryGetValue(requestedPath, out var requests))
            {
                foreach (var request in requests)
                {
                    request.Invoke(spriteAtlas);
                }
                _requestsByPath[requestedPath].Clear();
            }
        }

        private void PreloadReady (OpenSpriteAtlas spriteAtlas)
        {
            // nothing
        }

        private void Init()
        {
            if (_initialised) return;

            for (int i = 0; i < preloadAtlases.Length; i++)
            {
                RequestSpriteAtlas(preloadAtlases[i], PreloadReady);
            }

            _instance = this;
            _initialised = true;
        }

        private void Start()
        {
            Init();
        }

        private static string[] FindFiles(string path, string[] searchPatterns, bool recursive = false)
        {
            var files = new List<string>();
            if (!Directory.Exists(path)) return files.ToArray();
            foreach (var searchPattern in searchPatterns)
            {
                files.AddRange(Directory.GetFiles(path, searchPattern));
            }
            if (recursive)
            {
                var dirs = Directory.GetDirectories(path);
                foreach (var dir in dirs)
                {
                    files.AddRange(FindFiles(dir, searchPatterns, true));
                }
            }
            return files.ToArray();
        }
        private static string FullPath(string path, bool isDirectory = false)
        {
            switch (isDirectory)
            {
                case false when File.Exists(path):
                    return path;
                case true when Directory.Exists(path):
                    return path;
            }

            var alternativePath = Application.streamingAssetsPath + Path.DirectorySeparatorChar + path;
            switch (isDirectory)
            {
                case false when File.Exists(alternativePath):
                    return alternativePath;
                case true when Directory.Exists(alternativePath):
                    return alternativePath;
            }

            alternativePath = Application.persistentDataPath + Path.DirectorySeparatorChar + path;
            switch (isDirectory)
            {
                case false when File.Exists(alternativePath):
                    return alternativePath;
                case true when Directory.Exists(alternativePath):
                    return alternativePath;
            }

            alternativePath = Application.dataPath + Path.DirectorySeparatorChar + path;
            return isDirectory switch
            {
                false when File.Exists(alternativePath) => alternativePath,
                true when Directory.Exists(alternativePath) => alternativePath,
                _ => path
            };
        }
    }
}

